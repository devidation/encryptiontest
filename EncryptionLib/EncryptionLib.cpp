#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include "Base64.h"
#include <assert.h>

int padding = RSA_PKCS1_PADDING;

RSA* createRSA(unsigned char* key, bool isPublicKey)
{
	RSA* rsa = NULL;
	BIO* keybio;
	keybio = BIO_new_mem_buf(key, -1);
	if (keybio == NULL)
	{
		printf("Failed to create key BIO");
		return 0;
	}
	if (isPublicKey)
	{
		rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa, NULL, NULL);
	}
	else
	{
		rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL);
	}
	return rsa;

}
RSA* makeRSA(const std::string& key, bool isPublicKey)
{
	/*RSA* rsa = NULL;
	BIO* keybio;
	keybio = BIO_new_mem_buf(key.c_str(), -1);
	if (keybio == NULL)
	{
		printf("Failed to create key BIO");
		return 0;
	}
	if (isPublicKey)
	{
		rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa, NULL, NULL);
	}
	else
	{
		rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL);
	}
	if (rsa == NULL)
	{
		printf("Failed to create RSA");
	}

	return rsa;*/

	return createRSA((unsigned char*)key.c_str(),1);
}



struct resultRSA
{
	int length = -1;
	std::string data = std::string();
};

/*resultRSA encryptRSAPublic(const std::string data, const std::string publicKey, RSA* rsa)
{
	resultRSA result;
	const bool isPublic = true;
	unsigned char* encryptedData = new unsigned char[4096];
	//RSA* rsa = makeRSA(publicKey, isPublic);
	/*RSA* rsa = createRSA((unsigned char*)publicKey.c_str(), 1);
	if (rsa == NULL)
	{
		printf("Failed to create RSA for public key");
		return result;
	}

	int length = RSA_public_encrypt(data.length(), (unsigned char*)data.c_str(), encryptedData, rsa, RSA_PKCS1_PADDING);
	result.length = length;
	result.data = std::string((char*)encryptedData);
	return result;
}*/

/*resultRSA decryptRSAPrivate(const std::string data, const std::string privateKey, RSA* rsa)
{
	resultRSA result;
	const bool isPublic = false;
	unsigned char* decryptionResult = new unsigned char[4096];
	/*RSA* rsa = createRSA((unsigned char*)privateKey.c_str(), isPublic);
	if (rsa == NULL)
	{
		printf("Failed to create RSA for private key");
		return result;
	}

	auto size = RSA_size(rsa);
	int  length = RSA_private_decrypt(size, (unsigned char*)data.c_str(), decryptionResult, rsa, RSA_PKCS1_PADDING);
	result.length = length;
	result.data = std::string((char*)decryptionResult);
	return result;
}*/

int encryptRSAPublic(unsigned char* decryptedData, int dataLength, unsigned char* publicKey, unsigned char* encryptedData)
{
	const bool isPublicKey = true;
	RSA* rsa = createRSA(publicKey, isPublicKey);
	int result = RSA_public_encrypt(dataLength, decryptedData, encryptedData, rsa, RSA_PKCS1_PADDING);
	return result;
}

/*int encryptRSAPublic(std::string decryptedData, int dataLength, std::string publicKey, unsigned char* encryptedData)
{
	return encryptRSAPublic((unsigned char*)decryptedData.c_str(), dataLength, (unsigned char*)publicKey.c_str(), encryptedData);
}*/


int decryptRSAPrivate(unsigned char* encryptedData, unsigned char* privateKey, unsigned char* decryptedData)
{
	const bool isPublicKey = false;
	RSA* rsa = createRSA(privateKey, isPublicKey);
	auto keySize = RSA_size(rsa);
	int  result = RSA_private_decrypt(keySize, encryptedData, decryptedData, rsa, RSA_PKCS1_PADDING);
	return result;
}

/*int decryptRSAPrivate(std::string encryptedData,std::string privateKey, unsigned char* decryptedData)
{
	return decryptRSAPrivate((unsigned char*)encryptedData.c_str(), (unsigned char*)privateKey.c_str(), decryptedData);
}*/

int private_encrypt(unsigned char* data, int data_len, unsigned char* key, unsigned char* encrypted)
{
	RSA* rsa = createRSA(key, 0);
	int result = RSA_private_encrypt(data_len, data, encrypted, rsa, padding);
	return result;
}
int public_decrypt(unsigned char* enc_data, int data_len, unsigned char* key, unsigned char* decrypted)
{
	RSA* rsa = createRSA(key, 1);
	int  result = RSA_public_decrypt(data_len, enc_data, decrypted, rsa, padding);
	return result;
}

std::shared_ptr<char> decodeBase64(const std::string& input)
{
	BIO* bio, * b64;
	int padding = 0;
	if (input.at(input.length() - 1) == '=' && input.at(input.length() - 2) == '=')
	{
		padding = 2;
	}
	else if (input.at(input.length() - 1) == '=')
	{
		padding = 1;
	}
	int decodeLen = (int)input.length() * 0.75 - padding;
	int len = 0;
	std::shared_ptr<char> buffer(new char[decodeLen + 1]);

	b64 = BIO_new(BIO_f_base64());
	bio = BIO_new_mem_buf((unsigned char*)input.c_str(), input.length());
	bio = BIO_push(b64, bio);
	BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
	len = BIO_read(bio, buffer.get(), input.length());
	(buffer.get())[len] = '\0';

	BIO_free_all(bio);
	return buffer;
}

std::string decodeBase64AsString(const std::string& input)
{
	auto buffer = decodeBase64(input);
	std::string result(buffer.get());
	return result;
}



/*std::string decodeBase64(const std::string& input)
{
	BIO* bio, * b64;
	int padding = 0;
	if (input.at(input.length() - 1) == '=' && input.at(input.length() - 2) == '=')
	{
		padding = 2;
	}
	else if (input.at(input.length() - 1) == '=')
	{
		padding = 1;
	}
	int decodeLen = (int)input.length() * 0.75 - padding;
	int len = 0;
	char* buffer = new char[decodeLen + 1];

	b64 = BIO_new(BIO_f_base64());
	bio = BIO_new_mem_buf((unsigned char*)input.c_str(), input.length());
	bio = BIO_push(b64, bio);
	BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
	len = BIO_read(bio, buffer, input.length());
	(buffer)[len] = '\0';

	BIO_free_all(bio);
	std::string retval(buffer);
	delete[] buffer;
	return retval;
}*/


/*int Base64Decode(char* b64message, unsigned char** buffer, size_t* length) { //Decodes a base64 encoded string
	BIO* bio, * b64;

	int decodeLen = calcDecodeLength(b64message);
	*buffer = (unsigned char*)malloc(decodeLen + 1);
	(*buffer)[decodeLen] = '\0';

	bio = BIO_new_mem_buf(b64message, -1);
	b64 = BIO_new(BIO_f_base64());
	bio = BIO_push(b64, bio);

	BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Do not use newlines to flush buffer
	*length = BIO_read(bio, *buffer, strlen(b64message));
	BIO_free_all(bio);

	return (0); //success
}*/

void printLastError()
{
	char buffer[120];
	ERR_error_string(ERR_get_error(), buffer);
	fprintf(stderr, "OpenSSL error: %s", buffer);
	exit(0);
}

const std::string getFileContent(const std::string& path)
{
	std::ifstream fileStream(path);
	return std::string((std::istreambuf_iterator<char>(fileStream)), std::istreambuf_iterator<char>());
}


int main() {

	//char plainText[2048 / 8] = "Hello this is Ravi"; //key length : 2048

	/*std::string test1 = "Hello";
	const std::string& test2 = test1;
	const char* test1ptr = test1.c_str();
	const char* test2ptr = test2.c_str();*/

	std::string plainText = "Hello this is Ravi";

//////////////////////
//OWN GENERATED KEYS//
//////////////////////
	std::string base64EncodedData = "cmYgRLsMJDbXY4IT339bl207Di0ZKjdvl1oA+SCa6Qk3F1kJ8qLB2XYYvX/2DcpL"
		"JuOHYUyb3GIyGw7QJ78fvuHowfiULULNo4Qnp7QtASZ1UeUqyP1i28RUX1FftM3Y"
		"aqGxZzM2By1xSiTjOakgc9L+zKpoX7j10AQRagrBrqxiNcyv2yb0td31r7Mviu6w"
		"hOp+4DSC/4rgcBh9Ig9/nJRpjI+yClpJBxUNTgqyK7t9dz46t3boqLEa1fDUhBs3"
		"BIaxyvvZ3I5bYpAHDaRqkFTvvxdK90JVrGFQUDhEupHa9htSbxkLrGcBJbML/oNl"
		"7y97xWPiPni1RhFPyn1wLQ==";

	char publicKey[] = "-----BEGIN PUBLIC KEY-----"
		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr0Has4LhlPgOcbta7nd+"
		"7+h54izhLus66RGbCDgL80fbbnhmOi8vqiEiyvBzdGeJ/Y1NQi4ZSX0D4jWcnmWB"
		"QtXFlbFs9L9xIDD43ave+11IgRNW0jTg+o079bzsQygknJxArRhJD67sfdZ1qd8g"
		"OUykUlzvX7pvfXe1r2zAaO/cnaCFOfKlWBrdSaH4Qs2TYUpf41GUJB5872XG2q71"
		"G/uFgtlZd9AWx1Ypfewhx3GXaQKDPMt++pR06AG/yeBAh/HEdnou2+iekH4XWxyZ"
		"5T3YdRc1yxbttIvv3W5ErzOMrOnjNAuBeFr8dQ4WCRWIEqeuCac9apvtd/vZD0zf"
		"lQIDAQAB"
		"-----END PUBLIC KEY-----";

	std::string publicKeyAsString = "-----BEGIN PUBLIC KEY-----\n"
		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAr0Has4LhlPgOcbta7nd+\n"
		"7+h54izhLus66RGbCDgL80fbbnhmOi8vqiEiyvBzdGeJ/Y1NQi4ZSX0D4jWcnmWB\n"
		"QtXFlbFs9L9xIDD43ave+11IgRNW0jTg+o079bzsQygknJxArRhJD67sfdZ1qd8g\n"
		"OUykUlzvX7pvfXe1r2zAaO/cnaCFOfKlWBrdSaH4Qs2TYUpf41GUJB5872XG2q71\n"
		"G/uFgtlZd9AWx1Ypfewhx3GXaQKDPMt++pR06AG/yeBAh/HEdnou2+iekH4XWxyZ\n"
		"5T3YdRc1yxbttIvv3W5ErzOMrOnjNAuBeFr8dQ4WCRWIEqeuCac9apvtd/vZD0zf\n"
		"lQIDAQAB\n"
		"-----END PUBLIC KEY-----\n";

	std::string privateKeyAsString = "-----BEGIN RSA PRIVATE KEY-----\n"
		"MIIEpQIBAAKCAQEAr0Has4LhlPgOcbta7nd+7+h54izhLus66RGbCDgL80fbbnhm\n"
		"Oi8vqiEiyvBzdGeJ/Y1NQi4ZSX0D4jWcnmWBQtXFlbFs9L9xIDD43ave+11IgRNW\n"
		"0jTg+o079bzsQygknJxArRhJD67sfdZ1qd8gOUykUlzvX7pvfXe1r2zAaO/cnaCF\n"
		"OfKlWBrdSaH4Qs2TYUpf41GUJB5872XG2q71G/uFgtlZd9AWx1Ypfewhx3GXaQKD\n"
		"PMt++pR06AG/yeBAh/HEdnou2+iekH4XWxyZ5T3YdRc1yxbttIvv3W5ErzOMrOnj\n"
		"NAuBeFr8dQ4WCRWIEqeuCac9apvtd/vZD0zflQIDAQABAoIBAQCumXITwwGQEuz4\n"
		"+4mk56GryzSaUtHol2syRVrpfkTRkzYWQZa+wsG+wZJTEehEAN7lnr9oflqs7wJO\n"
		"e9DTsRZtCmH1NzTwREWkp4VX+mGElTOlfCnT8+BrXQduU1Ahu7111OlOSVKIcGGl\n"
		"IGJK4ULOMv/GGlZUTX3DRt7XrQWnxCE+q9P2j7TiZ0/tYLTZpCyU1Yy7J6bLffnm\n"
		"dihJknIYWnUkceiXMR/gZwMf37lg9DiuR6azr3yfaOzVB1UlArd/tqlElgvhdi7C\n"
		"AkJBcFDrGx26g5OYyF6Aa3SUuG6EjKyvOMOnSXRvIH1hK9v2hQncCbnCjop+fXd8\n"
		"E/TuoeAhAoGBANR56P36GvYLdKObh5oJLgSqUNV2/UhqGMVxh3TG/l9xXMB3F5dK\n"
		"Elq5FzyE9XHwSW/bOKc1HrvzsKsNH81SEkIZ38+I75PN2QHdCZmi3J2FYDg9fht9\n"
		"4zfYdZQJUQ8DO9gf40EKmYv+M+mhMGOxqf8ber1rpaXxdVmMytcDE+TJAoGBANMo\n"
		"NcFrH2+FgHAvk6UQEcPKw1j+UaE5tCQHTcsCspbrBNk+Lveo0tAo9ZpF7n/AdNRM\n"
		"nCZNa1IW6JIwp962ji/a8P1lZf+Nu7w1ato7FqFwam4jCh9araREdkqjfKjIJnwW\n"
		"F1xLh3hX0ElDTRrUCcwMTJIXQlVh3lXOM9K7WcZtAoGBAMODfmqeBssbFXQq/Gc4\n"
		"RTkTw5f5iu1ke3DLm9pP9ee7N57+tx2GkkcbBA+zROtYMSsVbRcnYSWQAqQTSo0z\n"
		"FQvwJSsiFVpLU4FMdLYDz7++6CA4LV98wmUz8FWNESKgViP0i7fg2hOgkQJNSWqj\n"
		"JAtTrOi6Mk31VodVb5zT/y1JAoGBALRnTkYJ97mYHbu7ajFEIWld+rg10Bgm7Lk7\n"
		"ZENl8p7t/B5++jGtky81T2CXDsUm8KnRKjevPVnNKLZ9y26EGcbZD7vuvc4wkYyj\n"
		"Nr9sbFFRchzW4vcWyfPmagoq036AQiER3rOD9CHlJJbzYRTiw9wJRgIqzQmVrBcz\n"
		"LSOF3ZWhAoGAaXnGS9gCz8qooamGV+A9Rts5GPRmAqFn8MFNVoHvCHJboWsmDdI5\n"
		"N61lRbw71Kub+Tiuoy+9s/zHOudmBydMLGaBwhJBNhH97uSwEThJP85shkgTZYH4\n"
		"ijcMjgR10pkl88r/odk6RcAAC60hUsy7jexqjx5rfEgQ+9ZZcfs9LdU=\n"
		"-----END RSA PRIVATE KEY-----\n";

	char privateKey[] = "-----BEGIN RSA PRIVATE KEY-----\n"
		"MIIEpQIBAAKCAQEAr0Has4LhlPgOcbta7nd+7+h54izhLus66RGbCDgL80fbbnhm\n"
		"Oi8vqiEiyvBzdGeJ/Y1NQi4ZSX0D4jWcnmWBQtXFlbFs9L9xIDD43ave+11IgRNW\n"
		"0jTg+o079bzsQygknJxArRhJD67sfdZ1qd8gOUykUlzvX7pvfXe1r2zAaO/cnaCF\n"
		"OfKlWBrdSaH4Qs2TYUpf41GUJB5872XG2q71G/uFgtlZd9AWx1Ypfewhx3GXaQKD\n"
		"PMt++pR06AG/yeBAh/HEdnou2+iekH4XWxyZ5T3YdRc1yxbttIvv3W5ErzOMrOnj\n"
		"NAuBeFr8dQ4WCRWIEqeuCac9apvtd/vZD0zflQIDAQABAoIBAQCumXITwwGQEuz4\n"
		"+4mk56GryzSaUtHol2syRVrpfkTRkzYWQZa+wsG+wZJTEehEAN7lnr9oflqs7wJO\n"
		"e9DTsRZtCmH1NzTwREWkp4VX+mGElTOlfCnT8+BrXQduU1Ahu7111OlOSVKIcGGl\n"
		"IGJK4ULOMv/GGlZUTX3DRt7XrQWnxCE+q9P2j7TiZ0/tYLTZpCyU1Yy7J6bLffnm\n"
		"dihJknIYWnUkceiXMR/gZwMf37lg9DiuR6azr3yfaOzVB1UlArd/tqlElgvhdi7C\n"
		"AkJBcFDrGx26g5OYyF6Aa3SUuG6EjKyvOMOnSXRvIH1hK9v2hQncCbnCjop+fXd8\n"
		"E/TuoeAhAoGBANR56P36GvYLdKObh5oJLgSqUNV2/UhqGMVxh3TG/l9xXMB3F5dK\n"
		"Elq5FzyE9XHwSW/bOKc1HrvzsKsNH81SEkIZ38+I75PN2QHdCZmi3J2FYDg9fht9\n"
		"4zfYdZQJUQ8DO9gf40EKmYv+M+mhMGOxqf8ber1rpaXxdVmMytcDE+TJAoGBANMo\n"
		"NcFrH2+FgHAvk6UQEcPKw1j+UaE5tCQHTcsCspbrBNk+Lveo0tAo9ZpF7n/AdNRM\n"
		"nCZNa1IW6JIwp962ji/a8P1lZf+Nu7w1ato7FqFwam4jCh9araREdkqjfKjIJnwW\n"
		"F1xLh3hX0ElDTRrUCcwMTJIXQlVh3lXOM9K7WcZtAoGBAMODfmqeBssbFXQq/Gc4\n"
		"RTkTw5f5iu1ke3DLm9pP9ee7N57+tx2GkkcbBA+zROtYMSsVbRcnYSWQAqQTSo0z\n"
		"FQvwJSsiFVpLU4FMdLYDz7++6CA4LV98wmUz8FWNESKgViP0i7fg2hOgkQJNSWqj\n"
		"JAtTrOi6Mk31VodVb5zT/y1JAoGBALRnTkYJ97mYHbu7ajFEIWld+rg10Bgm7Lk7\n"
		"ZENl8p7t/B5++jGtky81T2CXDsUm8KnRKjevPVnNKLZ9y26EGcbZD7vuvc4wkYyj\n"
		"Nr9sbFFRchzW4vcWyfPmagoq036AQiER3rOD9CHlJJbzYRTiw9wJRgIqzQmVrBcz\n"
		"LSOF3ZWhAoGAaXnGS9gCz8qooamGV+A9Rts5GPRmAqFn8MFNVoHvCHJboWsmDdI5\n"
		"N61lRbw71Kub+Tiuoy+9s/zHOudmBydMLGaBwhJBNhH97uSwEThJP85shkgTZYH4\n"
		"ijcMjgR10pkl88r/odk6RcAAC60hUsy7jexqjx5rfEgQ+9ZZcfs9LdU=\n"
		"-----END RSA PRIVATE KEY-----\n";

//////////////////////
//EXAMPLE KEYS      //
//////////////////////
	/*char publicKey[] = "-----BEGIN PUBLIC KEY-----\n"\
		"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy8Dbv8prpJ/0kKhlGeJY\n"\
		"ozo2t60EG8L0561g13R29LvMR5hyvGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+\n"\
		"vw1HocOAZtWK0z3r26uA8kQYOKX9Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQAp\n"\
		"fc9jB9nTzphOgM4JiEYvlV8FLhg9yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68\n"\
		"i6T4nNq7NWC+UNVjQHxNQMQMzU6lWCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoV\n"\
		"PpY72+eVthKzpMeyHkBn7ciumk5qgLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUy\n"\
		"wQIDAQAB\n"\
		"-----END PUBLIC KEY-----\n";



	char privateKey[] = "-----BEGIN RSA PRIVATE KEY-----\n"\
		"MIIEowIBAAKCAQEAy8Dbv8prpJ/0kKhlGeJYozo2t60EG8L0561g13R29LvMR5hy\n"\
		"vGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+vw1HocOAZtWK0z3r26uA8kQYOKX9\n"\
		"Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQApfc9jB9nTzphOgM4JiEYvlV8FLhg9\n"\
		"yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68i6T4nNq7NWC+UNVjQHxNQMQMzU6l\n"\
		"WCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoVPpY72+eVthKzpMeyHkBn7ciumk5q\n"\
		"gLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUywQIDAQABAoIBADhg1u1Mv1hAAlX8\n"\
		"omz1Gn2f4AAW2aos2cM5UDCNw1SYmj+9SRIkaxjRsE/C4o9sw1oxrg1/z6kajV0e\n"\
		"N/t008FdlVKHXAIYWF93JMoVvIpMmT8jft6AN/y3NMpivgt2inmmEJZYNioFJKZG\n"\
		"X+/vKYvsVISZm2fw8NfnKvAQK55yu+GRWBZGOeS9K+LbYvOwcrjKhHz66m4bedKd\n"\
		"gVAix6NE5iwmjNXktSQlJMCjbtdNXg/xo1/G4kG2p/MO1HLcKfe1N5FgBiXj3Qjl\n"\
		"vgvjJZkh1as2KTgaPOBqZaP03738VnYg23ISyvfT/teArVGtxrmFP7939EvJFKpF\n"\
		"1wTxuDkCgYEA7t0DR37zt+dEJy+5vm7zSmN97VenwQJFWMiulkHGa0yU3lLasxxu\n"\
		"m0oUtndIjenIvSx6t3Y+agK2F3EPbb0AZ5wZ1p1IXs4vktgeQwSSBdqcM8LZFDvZ\n"\
		"uPboQnJoRdIkd62XnP5ekIEIBAfOp8v2wFpSfE7nNH2u4CpAXNSF9HsCgYEA2l8D\n"\
		"JrDE5m9Kkn+J4l+AdGfeBL1igPF3DnuPoV67BpgiaAgI4h25UJzXiDKKoa706S0D\n"\
		"4XB74zOLX11MaGPMIdhlG+SgeQfNoC5lE4ZWXNyESJH1SVgRGT9nBC2vtL6bxCVV\n"\
		"WBkTeC5D6c/QXcai6yw6OYyNNdp0uznKURe1xvMCgYBVYYcEjWqMuAvyferFGV+5\n"\
		"nWqr5gM+yJMFM2bEqupD/HHSLoeiMm2O8KIKvwSeRYzNohKTdZ7FwgZYxr8fGMoG\n"\
		"PxQ1VK9DxCvZL4tRpVaU5Rmknud9hg9DQG6xIbgIDR+f79sb8QjYWmcFGc1SyWOA\n"\
		"SkjlykZ2yt4xnqi3BfiD9QKBgGqLgRYXmXp1QoVIBRaWUi55nzHg1XbkWZqPXvz1\n"\
		"I3uMLv1jLjJlHk3euKqTPmC05HoApKwSHeA0/gOBmg404xyAYJTDcCidTg6hlF96\n"\
		"ZBja3xApZuxqM62F6dV4FQqzFX0WWhWp5n301N33r0qR6FumMKJzmVJ1TA8tmzEF\n"\
		"yINRAoGBAJqioYs8rK6eXzA8ywYLjqTLu/yQSLBn/4ta36K8DyCoLNlNxSuox+A5\n"\
		"w6z2vEfRVQDq4Hm4vBzjdi3QfYLNkTiTqLcvgWZ+eX44ogXtdTDO7c+GeMKWz4XX\n"\
		"uJSUVL5+CVjKLjZEJ6Qc2WZLl94xSwL71E41H4YciVnSCQxVc4Jw\n"\
		"-----END RSA PRIVATE KEY-----\n";*/


	//RSA* rsapub = createRSA((unsigned char*)publicKeyAsString.c_str(), 1);
	//RSA* rsapriv = createRSA((unsigned char*)privateKeyAsString.c_str(), 0);
	//auto encryptionResult = encryptRSAPublic(plainText, publicKeyAsString,rsapub);
	//auto decryptionResult = decryptRSAPrivate(encryptionResult.data, privateKeyAsString,rsapriv);
	//printf("Decrypted Text =%s\n", decryptionResult.data);

	////////////////////////////////////////
	//Test basic encryption and decryption//
	////////////////////////////////////////

	unsigned char* encrypted = new unsigned char[4098];
	unsigned char decrypted[4098] = {};
	//int encrypted_length = encryptRSAPublic(plainText, plainText.length(), publicKeyAsString, encrypted);

	int encrypted_length = encryptRSAPublic((unsigned char*)plainText.c_str(), plainText.length(), (unsigned char*)publicKeyAsString.c_str(), encrypted);
	if (encrypted_length == -1)
	{
		printLastError();
		exit(0);
	}
	printf("Encrypted length =%d\n", encrypted_length);

	//std::string encAsString(reinterpret_cast<const char*>(encrypted));
	//int decrypted_length = decryptRSAPrivate(encAsString, privateKeyAsString, decrypted);

	int decrypted_length = decryptRSAPrivate(encrypted, (unsigned char*)privateKeyAsString.c_str(), decrypted);
	if (decrypted_length == -1)
	{
		printLastError();
		exit(0);
	}

	std::string s(reinterpret_cast<const char*>(decrypted));
	std::cout << "Decrypted text = " << s << std::endl;
	printf("Decrypted Length =%d\n", decrypted_length);

	//////////////////////////////
	//Test Base64 encrypted data//
	//////////////////////////////

	auto buffer = decodeBase64(base64EncodedData);
	decrypted_length = decryptRSAPrivate((unsigned char*)buffer.get(), (unsigned char*)privateKeyAsString.c_str(), decrypted);
	if (decrypted_length == -1)
	{
		printLastError();
		exit(0);
	}
	printf("Decrypted Text =%s\n", decrypted);
	printf("Decrypted Length =%d\n", decrypted_length);

	//////////////////////////////////
	//Decrypt with privkey from file//
	//////////////////////////////////

	std::string privKeyFromfile = getFileContent("res/privkey.pem");
	auto binaryData = decodeBase64(base64EncodedData);
	decrypted_length = decryptRSAPrivate((unsigned char*)binaryData.get(), (unsigned char*)privKeyFromfile.c_str(), decrypted);
	if (decrypted_length == -1)
	{
		printLastError();
		exit(0);
	}
	printf("Decrypted Text =%s\n", decrypted);
	printf("Decrypted Length =%d\n", decrypted_length);

	//auto encryptionResult = encryptRSAPublic(plainText, publicKeyAsString);
	//auto decryptionResult = decryptRSAPrivate(encryptionResult.data, privateKeyAsString);


	//////////////
	//OLD DECODE//
	//////////////
	auto res = base64::decode(base64EncodedData);
	char* reschars = new char[res.size() + 1]; // +1 for the final 0
	for (size_t i = 0; i < res.size(); ++i) {
		reschars[i] = res[i];
	}

	decrypted_length = decryptRSAPrivate((unsigned char*)reschars, (unsigned char*)privateKeyAsString.c_str(), decrypted);
	if (decrypted_length == -1)
	{
		printLastError();
		exit(0);
	}
	printf("Decrypted Text =%s\n", decrypted);
	printf("Decrypted Length =%d\n", decrypted_length);


	

	

	std::cin.get();

	//unsigned char  encrypted[4098] = {};


	/*auto res = base64::decode(base64EncodedData);


	std::ifstream f("res/privkey.pem");
	if (f.good())
	{
		std::cout << "exists" << std::endl;
	}
	else
	{
		std::cout << "doesn't exist" << std::endl;
	}

	FILE* fp = fopen("res/privkey.pem", "r");
	//printFile(fp);

	RSA* rsa = PEM_read_RSAPrivateKey(fp, NULL, NULL, NULL);
	if (rsa == NULL)
	{
		std::cout << "RSA IS NULL" << std::endl;
	}
	else
	{
		std::cout << "RSA OK" << std::endl;
	}
	fclose(fp);

	std::ifstream enc_file;

	int keysize = RSA_size(rsa);
	enc_file.open("res/encrypted_file", std::ifstream::binary);
	std::auto_ptr<unsigned char> rsa_in(new unsigned char[keysize * 2]);

	memset(rsa_in.get(), 0, keysize * 2);

	enc_file.read(reinterpret_cast<char*>(rsa_in.get()), keysize * 2);
	int rsa_inlen = enc_file.gcount();




	encrypted_length = private_encrypt((unsigned char*)plainText.c_str(), plainText.length(), (unsigned char*)privateKey, encrypted);
	if (encrypted_length == -1)
	{
		printLastError();
		exit(0);
	}
	printf("Encrypted length =%d\n", encrypted_length);

	decrypted_length = public_decrypt(encrypted, encrypted_length, (unsigned char*)publicKey, decrypted);
	if (decrypted_length == -1)
	{
		printLastError();
		exit(0);
	}
	printf("Decrypted Text =%s\n", decrypted);
	printf("Decrypted Length =%d\n", decrypted_length);*/
}

/*#include <fstream>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <iostream>
#include <openssl/err.h>

std::string private_key_file_name = "res/privkey.pem";
std::string encrypted_file_name = "res/encrypted_file";
std::string decrypted_file_name = "res/decrypted_file";

void printFile(FILE* fp)
{
	FILE f = *fp;
	FILE* fPointer = &f;
	int c;
		while ((c = getc(fPointer)) != EOF)
			putchar(c);
}

int main()
{
	std::ifstream f("res/privkey.pem");
	if (f.good())
	{
		std::cout << "exists" << std::endl;
	}
	else
	{
		std::cout << "doesn't exist" << std::endl;
	}

	FILE* fp = fopen("res/privkey.pem", "r");
	//printFile(fp);

	RSA* rsa = PEM_read_RSAPrivateKey(fp, NULL, NULL, NULL);
	if (rsa == NULL)
	{
		std::cout << "RSA IS NULL" << std::endl;
	}
	else
	{
		std::cout << "RSA OK" << std::endl;
	}
	fclose(fp);

	std::ifstream enc_file;

	enc_file.open(encrypted_file_name, std::ifstream::in);
	//std::cout << enc_file.rdbuf() << std::endl;

	int keysize = RSA_size(rsa);

	std::auto_ptr<unsigned char> rsa_in(new unsigned char[keysize * 2]);
	std::auto_ptr<unsigned char> rsa_out(new unsigned char[keysize]);

	memset(rsa_in.get(), 0, keysize * 2);
	memset(rsa_out.get(), 0, keysize);

	enc_file.read(reinterpret_cast<char*>(rsa_in.get()), keysize * 2);

	int rsa_inlen = enc_file.gcount();

	int rsa_outlen = RSA_private_decrypt(
		256, rsa_in.get(), rsa_out.get(),
		rsa, RSA_);

	if (rsa_outlen == -1)
	{
		char buffer[120];
		ERR_error_string(ERR_get_error(), buffer);
		fprintf(stderr, "OpenSSL error: %s", buffer);
		exit(0);
	}
	std::ofstream dec_file;

	dec_file.open(decrypted_file_name, std::ifstream::out);

	dec_file.write(reinterpret_cast<char*>(rsa_out.get()), rsa_outlen);

	enc_file.close();
	dec_file.close();

	RSA_free(rsa);

	std::cin.get();
	return 0;
}*/